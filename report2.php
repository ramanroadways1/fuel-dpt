<?php 
require_once './connect.php';

$from = $_POST['from'];
$to = $_POST['to'];
$selection = $_POST['selection'];

$output = '';

if($selection=='M')
{
$query = mysqli_query($conn,"SELECT d.branch,d.fno,d.qty,d.rate,d.disamt,d.cash,d.tno,d.lrno,d.type,d.dsl_by,d.dcard,d.dcom,d.dsl_nrr,
d.pay_date,d.done,p.name,p.comp FROM diesel_fm as d 
LEFT OUTER JOIN diesel_pump as p ON p.code=d.dcard 
WHERE d.pay_date BETWEEN '$from' AND '$to' ORDER BY d.id ASC");

if(!$query)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($query) > 0)
 {
 $output .= '
   <table border="1">  
                    <tr>  
                         <th>Branch</th>  
                         <th>Token No</th>  
                         <th>TruckNo</th>  
                         <th>LR_No</th>  
                         <th>Qty</th>  
                         <th>Rate</th>  
                         <th>Amount</th>  
                         <th>Adv/Bal</th>  
                         <th>Card/Pump</th>  
                         <th>CardNo</th>  
                         <th>FuelCompany</th>  
                         <th>Narration</th>  
                         <th>Date</th>  
                         <th>Status</th>  
                         <th>PumpName</th>  
                         <th>POS</th>  
                    </tr>
  ';
  while($row = mysqli_fetch_array($query))
  {
	 if($row['done']==1)
	 {
		 $status="<b><font color='green'>Done</font></b>";
	 }
	else	 
	{
		$status="<b><font color='red'>Pending</font></b>";
	}
	  
   $output .= '
    <tr>  
							<td>'.$row["branch"].'</td>  
							<td>'.$row["fno"].'</td>  
							<td>'.$row["tno"].'</td>
						   <td>'.$row["lrno"].'</td>
						   <td>'.$row["qty"].'</td>
						   <td>'.$row["rate"].'</td>
						   <td>'.$row["disamt"].'</td>  
						   <td>'.$row["type"].'</td>
						   <td>'.$row["dsl_by"].'</td>
						   <td>'.$row["dcard"].'</td>
						   <td>'.$row["dcom"].'</td>
						   <td>'.$row["dsl_nrr"].'</td>
						   <td>'.$row["pay_date"].'</td>
						   <td>'.$status.'</td>
						    <td>'.$row["name"].'</td>
							 <td>'.$row["comp"].'</td>
					</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Diesel_Trans_Market.xls');
  echo $output;
  
}
 else
 {
	 echo "<script>
			alert('No result found..');
			window.close();
		</script>";
 }

}
else // IF OWN TRUCK
{
	$query = mysqli_query($conn_db,"SELECT d.unq_id,d.tno,d.rate,d.qty,d.amount,d.date,de.card_pump,de.card,de.dsl_company,de.branch,
	de.done,p.name,p.comp FROM diesel AS d 
	LEFT OUTER JOIN diesel_entry as de ON de.id=d.id 
	LEFT OUTER JOIN diesel_pump_own as p ON p.code=de.card 
	WHERE d.date between '$from' and '$to' ORDER BY p.id ASC");

if(!$query)
{
	echo mysqli_error($conn_db);
	exit();
}

if(mysqli_num_rows($query) > 0)
 {
 $output .= '
   <table border="1">  
                    <tr>  
                        <th>Token No</th>  
						<th>Branch</th>  	
						<th>Truck No</th>                          
						<th>Qty</th>  
						<th>Rate</th>  
						<th>Amount</th>  
                        <th>Card/Pump</th>  
                        <th>Card_No</th>  
                        <th>FuelCompany</th>  
                        <th>Narration</th>  
                        <th>Date</th>  
                        <th>Status</th>  
                        <th>PumpName</th>  
                        <th>POS</th>  
                   </tr>
  ';
  while($row = mysqli_fetch_array($query))
  {
	 if($row['done']==1)
	 {
		 $status="<b><font color='green'>Done</font></b>";
	 }
	else	 
	{
		$status="<b><font color='red'>Pending</font></b>";
	}
	
   $output .= '
    <tr>  
							<td>'.$row["unq_id"].'</td>  
							<td>'.$row["branch"].'</td>  
							<td>'.$row["tno"].'</td>
						   <td>'.$row["qty"].'</td>
						   <td>'.$row["rate"].'</td>
						   <td>'.$row["amount"].'</td>
						   <td>'.$row["card_pump"].'</td>
						   <td>'.$row["card"].'</td>
						   <td>'.$row["dsl_company"].'</td>
						   <td>'.$row["dsl_company"].'-'.$row["card"].' Rs : '.$row["amount"].'</td>
						   <td>'.$row["date"].'</td>
						   <td>'.$status.'</td>
						   <td>'.$row["name"].'</td>
						   <td>'.$row["comp"].'</td>
					</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Diesel_Trans_Own.xls');
  echo $output;
  
}
 else
 {
	 echo "<script>
			alert('No result found..');
			window.close();
		</script>";
 }
}
?>