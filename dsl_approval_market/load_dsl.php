<?php 
require_once '../connect.php';

echo "<body style='font-family:Verdana'>";	

$qry = mysqli_query($conn,"SELECT id,branch,fno,disamt,tno,lrno,type,dsl_by,dsl_nrr,pay_date FROM diesel_fm WHERE done!='1' AND 
approval='1' AND dsl_by IN('CARD','OTP') AND disamt>0");
	
	if($qry)
	{
		if(mysqli_num_rows($qry)==0)
		{
			echo "<br />
			<font color='red' size='4'><center>No result found !</font></center>";	
		}
		else
		{
			echo "
			<br />
		<table class='table table-bordered' style='background:#FFF;font-family:Verdana;font-size:12px'>
			<tr>
				<th>Id</th>
				<th>Token No</th>
				<th>Branch</th>
				<th>Truck No</th>
				<th>LR No</th>
				<th>Adv/Bal</th>
				<th>Amount</th>
				<th>Type</th>
				<th>Narration</th>
				<th>Date</th>
				<th>Edit</th>
				<th>Done</th>
			</tr>
		";	
		$num = 1;	
			while($row = mysqli_fetch_array($qry))
			{
				echo "
				<td>$num</td>
				<td>$row[fno]</td>
				<td>$row[branch]</td>
				<td>$row[tno]</td>
				<td>$row[lrno]</td>
				<td>$row[type]</td>
				<td>$row[disamt]</td>
				<td>$row[dsl_by]</td>
				<td>$row[dsl_nrr]</td>
				<td>".date("d-m-y",strtotime($row['pay_date']))."</td>
				<td align='center'>
					<button type='button' onclick='edit($row[id])'>Edit</button>
				</td>
				<td align='center'>
					<button type='button' onclick='Done($row[id])'>Done</button>
				</td>
				</tr>
				";
			$num++;
			}
			echo "</table>";
			
		}
	
	}
	else
	{
		echo mysqli_error($conn);
		exit();
	}
?>