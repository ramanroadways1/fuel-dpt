<?php 
require_once '../connect.php';

$date2 = date("Y-m-d"); 
$max = date("Y-m-d");
$min = date("Y-m-d", strtotime("-30 day"));
?>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	 <style> 
.table-bordered > tbody > tr > th {
     border: 1px solid gray;
}

.table-bordered > tbody > tr > td {
     border: 1px solid gray;
}
input[type="text"]{border:1px solid #000;}
label{font-family:Verdana;font-size:12px;}
 </style> 

<div id="new" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1;">
	<center><img style="margin-top:150px" src="../load.gif" /></center>
</div>

<div id="result_1"></div>

<script type="text/javascript">
$(document).ready(function (e) {
$("#Update").on('submit',(function(e) {
$("#new").show();
e.preventDefault();
	$.ajax({
	url: "./update_data.php",
	type: "POST",
	data:  new FormData(this),
	contentType: false,
	cache: false,
	processData:false,
	success: function(data)
	{
	$("#result_1").html(data);
	$("#new").hide();
	},
	error: function() 
	{} });}));});
</script>

<script type="text/javascript">
function edit(idnew){
	$("#new").show();
		$('#id_val').val(idnew);
       jQuery.ajax({
		url: "fetch_data.php",
		data: 'id=' + idnew,
		type: "POST",
		success: function(data) {
		$("#result_1").html(data);
		},
		error: function() {}
	});
    }
</script>

<script type="text/javascript">
function Done(id){
	$("#new").show();
       jQuery.ajax({
		url: "done_diesel.php",
		data: 'id=' + id,
		type: "POST",
		success: function(data) {
		$("#result_1").html(data);
		},
		error: function() {}
	});
    }
</script>

</head>

<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content" style="font-family:Verdana">
        <div class="modal-header">
      <form action="" autocomplete="off" id="Update">   
		 <span style="font-size:16px;">Vou Id - </span><span style="color:brown" id="vou_id"></span>
		 &nbsp;,
		 <span style="font-size:16px;">Branch</span> - <span style="color:brown" id="branch_name"></span></h4>
        </div>
        <div class="modal-body">
		<div class="row">
          <input type="hidden" name="id" id="id_val" />
		  
          <input type="hidden" id="old_card" name="old_card" />
          <input type="hidden" id="old_comp" name="old_comp" />
          
		  <div class="form-group col-md-6">
			<label>Amount </label>
			<input type="text" style="border:1px solid #000" name="diesel_amount" id="diesel_amount" class="form-control" readonly required="required">
		 </div>
		 
		 <div class="form-group col-md-6">
			<label>Diesel By </label>
			<input type="text" style="border:1px solid #000" name="dsl_by" id="dsl_by" class="form-control" readonly required="required">
		 </div>
		 
		 <div class="form-group col-md-6">
			<label>Card No. </label>
			<input type="number" style="border:1px solid #000" oninput="SetNrr()" onblur="SetNrr()" style="text-transform:uppercase" name="dcard" id="dcard" class="form-control" required />
		 </div>
		 
		 <div class="form-group col-md-6">
			<label>Fuel Company. </label>
			<select name="dcom" onchange="SetNrr()" style="border:1px solid #000" id="dcom" class="form-control" required>
				<option value="">Select an option</option>
				<option value="HPCL">HPCL</option>
				<option value="BPCL">BPCL</option>
				<option value="IOCL">IOCL</option>
				<option value="RELIANCE">RELIANCE</option>
			</select>
		 </div>

<script>
function SetNrr()
{
	var comp=$('#dcom').val();
	var card=$('#dcard').val();
	var amount=$('#diesel_amount').val();
	
	$('#dsl_nrr').val(comp+"-"+card+" Rs:"+amount+"/-");
}
</script>		 
		
		 <div class="form-group col-md-12">
			<label>Narration. </label>
			<input type="text" name="dsl_nrr" id="dsl_nrr" class="form-control" readonly required />
		 </div>
		 
        </div>
        </div>
		
        <div class="modal-footer">
          <button type="submit" id="btn_submit" class="btn btn-danger">Update</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
		</div>
      </div>
      </form>
    </div>
  </div>

<body style="background-color:">
<div style="margin-top:15px;" class="pull-left">
<span style="font-size:16px;color:#000;font-family:Verdana;margin-left:15px;margin-top:20px;">
<b><font color="red">Market Truck</b></font> Diesel Download :</span>
</div>

<div class="pull-right" style="font-family:Verdana">
	<a href="../"><button style="margin-right:5px;margin-top:15px;margin-right:5px" class="btn btn-danger">Go back</button></a>
</div>

<script type="text/javascript">
function auto_load(){
	$("#new").show();
        $.ajax({
          url: "load_dsl.php",
          cache: false,
          success: function(data){
             $("#load1").html(data);
			 $("#new").hide();
          } 
        });
}
 $(document).ready(function(){
 auto_load();
 });
</script>

<div id="result_chk"></div>

<div class="container-fluid" style="font-family:Verdana">

	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12 table-responsive" id="load1">
		</div>
	</div>

</div>

</body>