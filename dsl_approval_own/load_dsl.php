<?php 
require_once '../connect.php';

echo "<body style='font-family:Verdana'>";	

$qry = mysqli_query($conn_db,"SELECT id,tno,diesel,card_pump,card,dsl_company,branch,date FROM diesel_entry WHERE done!=1 AND 
card_pump IN('CARD','OTP') ORDER BY id ASC"); 
 
	if($qry)
	{
		if(mysqli_num_rows($qry)==0)
		{
			echo "<br />
			<font color='red' size='4'><center>No result found !</font></center>";	
		}
		else
		{
			echo "
			<br />
		<table class='table table-bordered' style='background:#FFF;font-family:Verdana;font-size:12px'>
			<tr>
				<th>Id</th>
				<th>Branch</th>
				<th>Truck No</th>
				<th>Amount</th>
				<th>Type</th>
				<th>Card</th>
				<th>FuelComp</th>
				<th>Narration</th>
				<th>Date</th>
				<th>Done</th>
			</tr>
		";	
		$num = 1;	
			while($row = mysqli_fetch_array($qry))
			{
				echo "
				<td>$num</td>
				<td>$row[branch]</td>
				<td>$row[tno]</td>
				<td>$row[diesel]</td>
				<td>$row[card_pump]</td>
				<td>$row[card]</td>
				<td>$row[dsl_company]</td>
				<td>$row[dsl_company]-$row[card] Rs: $row[diesel]/-</td>
				<td>".date("d-m-y",strtotime($row['date']))."</td>
				<td align='center'>
					<button type='button' onclick='Done($row[id])'>Done</button>
				</td>
				</tr>
				";
			$num++;
			}
			echo "</table>";
		}
	}
	else
	{
		echo mysqli_error($conn_db);
		exit();
	}
	// <td align='center'>
					// <button type='button' onclick='edit($row[id])'>Edit</button>
				// </td>
?>