<?php 
require_once './connect.php';
?>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

 <style> 
.table-bordered > tbody > tr > th {
     border: 1px solid gray;
}

.table-bordered > tbody > tr > td {
     border: 1px solid gray;
}
input[type="text"]{border:1px solid #000;}
label{font-family:Verdana;font-size:12px;}
 </style> 

<div id="new" style="display:none;position: fixed; right: 0px; top: 0px; width: 100%;height: 100%; background-color:#FFF; z-index: 30001; opacity:1;">
	<center><img style="margin-top:150px" src="../load.gif" /></center>
</div>

<?php 
require_once './connect.php';

$from = $_POST['from'];
$to = $_POST['to'];
$selection = $_POST['selection'];

$output = '';

if($selection=='M')
{
$query = mysqli_query($conn,"SELECT branch,fno,qty,rate,disamt,cash,tno,lrno,type,dsl_by,dcard,dcom,dsl_nrr,pay_date,done FROM diesel_fm 
WHERE pay_date BETWEEN '$from' AND '$to' AND approval='1' ORDER BY id ASC");

if(!$query)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($query) > 0)
 {
 $output .= '
 <div class="container-fluid" style="font-family:Verdana">

	<div class="row">
	<a href="./dsl_db.php"><button style="margin:10px;" class="btn btn-danger">Go back</button></a>
	<form action="diesel_download.php" method="POST" target="_blank">
		<input type="hidden" value="'.$from.'" name="from">
		<input type="hidden" value="'.$to.'" name="to">
		<input type="hidden" value="'.$selection.'" name="selection">
		<button style="margin:10px;" type="submit" class="btn btn-primary pull-right">Download</button>
	</form>
	<br />
		<div class="form-group col-md-12 table-responsive">
   <table class="table table-bordered" style="font-size:13px;font-family:Verdana">  
                    <tr>  
                         <th>Id</th>  
                         <th>Branch</th>  
                         <th>Token No</th>  
                         <th>TruckNo</th>  
                         <th>LR_No</th>  
                         <th>Diesel</th>  
                         <th>Cash</th>  
                         <th>Adv/Bal</th>  
                         <th>Card/Pump</th>  
                         <th>CardNo</th>  
                         <th>FuelCompany</th>  
                         <th>Narration</th>  
                         <th>Date</th>  
                         <th>Status</th>  
                    </tr>
  ';
  $sn=1;
  while($row = mysqli_fetch_array($query))
  {
	 if($row['done']==1)
	 {
		 $status="<b><font color='green'>Done</font></b>";
	 }
	else	 
	{
		$status="<b><font color='red'>Pending</font></b>";
	}
	  
   $output .= '
    <tr>  
							<td>'.$sn.'</td>  
							<td>'.$row["branch"].'</td>  
							<td>'.$row["fno"].'</td>  
							<td>'.$row["tno"].'</td>
						   <td>'.$row["lrno"].'</td>
						   <td>'.$row["disamt"].'</td>  
						   <td>'.$row["cash"].'</td>
						   <td>'.$row["type"].'</td>
						   <td>'.$row["dsl_by"].'</td>
						   <td>'.$row["dcard"].'</td>
						   <td>'.$row["dcom"].'</td>
						   <td>'.$row["dsl_nrr"].'</td>
						   <td>'.$row["pay_date"].'</td>
						   <td>'.$status.'</td>
					</tr>
   ';
   $sn++;
  }
  $output .= '</table>
  </div>
  </div>
  </div>
  ';
    echo $output;
}
 else
 {
	 echo "<script>
			alert('No result found..');
			window.location.href='./dsl_db.php';
		</script>";
 }

}
else // IF OWN TRUCK
{
	$query = mysqli_query($conn_db,"SELECT unq_id,tno,diesel,card,dsl_company,card_pump,narration,branch,date,done FROM diesel_entry WHERE 
	date BETWEEN '$from' AND '$to' AND (card_pump='CARD' || card_pump='OTP') ORDER BY id ASC");

if(!$query)
{
	echo mysqli_error($conn_db);
	exit();
}

if(mysqli_num_rows($query) > 0)
 {
 $output .= '
 <div class="container-fluid" style="font-family:Verdana">

	<div class="row">
	<a href="./dsl_db.php"><button style="margin:10px;" class="btn btn-danger">Go back</button></a>
	
	<form action="diesel_download.php" method="POST" target="_blank">
		<input type="hidden" value="'.$from.'" name="from">
		<input type="hidden" value="'.$to.'" name="to">
		<input type="hidden" value="'.$selection.'" name="selection">
		<button style="margin:10px;" type="submit" class="btn btn-primary pull-right">Download</button>
	</form>
	
	<br />
		<div class="form-group col-md-12 table-responsive">
		
   <table class="table table-bordered" style="font-size:13px;font-family:Verdana">  
                    <tr>  
                         <th>Id</th>  
                         <th>Token No</th>  
						 <th>Branch</th>  	
						<th>Truck No</th>                          
						 <th>Amount</th>  
                        <th>Card/OTP</th>  
                        <th>Card_No</th>  
                        <th>FuelCompany</th>  
                         <th>Narration</th>  
                         <th>Date</th>  
                         <th>Status</th>  
                    </tr>
  ';
  $sn=1;
  while($row = mysqli_fetch_array($query))
  {
	 if($row['done']==1)
	 {
		 $status="<b><font color='green'>Done</font></b>";
	 }
	else	 
	{
		$status="<b><font color='red'>Pending</font></b>";
	}
	
   $output .= '
    <tr>  
							<td>'.$sn.'</td>  
							<td>'.$row["unq_id"].'</td>  
							<td>'.$row["branch"].'</td>  
							<td>'.$row["tno"].'</td>
						   <td>'.$row["diesel"].'</td>
						   <td>'.$row["card_pump"].'</td>
						   <td>'.$row["card"].'</td>
						   <td>'.$row["dsl_company"].'</td>
						   <td>'.$row["narration"].'</td>
						   <td>'.$row["date"].'</td>
						   <td>'.$status.'</td>
					</tr>
   ';
   $sn++;
  }
  $output .= '</table>
  </div>
  </div>
  </div>';
  echo $output;
  
}
 else
 {
	 echo "<script>
			alert('No result found..');
			window.location.href='./dsl_db.php';
		</script>";
 }
}
?>