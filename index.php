<?php
require_once 'connect.php';

$today=date('Y-m-d');

$count=mysqli_query($conn,"SELECT SUM(diesel_amount) AS diesel_amt,SUM(diesel) As count_diesel FROM today_data WHERE date='$today'"); 

$row=mysqli_fetch_assoc($count); 

$diesel_market=$row['diesel_amt'];
$diesel_market_count=$row['count_diesel'];

$qry2=mysqli_query($conn_db,"SELECT SUM(amount) AS diesel FROM diesel WHERE date='$today'");
$row11=mysqli_fetch_array($qry2);

$own_diesel=$row11['diesel'];
$own_diesel_count=mysqli_num_rows($qry2);

$chk_login = mysqli_query($conn,"SELECT password FROM user WHERE username='DIESEL'");
$row_login = mysqli_fetch_array($chk_login);

$chk_newPass=mysqli_query($conn,"SELECT hash FROM log_reset_password WHERE user_type='DIESEL' ORDER BY id DESC LIMIT 1");
$row_newpass = mysqli_fetch_array($chk_newPass);

if($row_newpass['hash']!=$row_login['password'])
{
	echo "<script>
		alert('Password Expired.')
		window.location.href='./logout.php';
	</script>";
	exit();
}
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Fuel USER : RRPL</title>
<meta http-equiv="refresh" content="60">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/styles.css" rel="stylesheet">
<script src="js/lumino.glyphs.js"></script>

<style>
.form-control
{
	border:1px solid #000;
	background:#FFF;
	text-transform:uppercase;
}
</style>

 <style> 
 label{
	 font-family:Verdana;
	 font-size:13px;
 }
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
 </style> 
</head>

<body style="background:lightblue">

<?php include 'sidebar.php';?>

<div class="container-fluid;font-family:Verdana">	
	
<div class="col-sm-10 col-sm-offset-2 col-lg-10 col-lg-offset-2">			
	<br />
	<br />
	<br />
	<div class="row">
			<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;">Rs.<?php echo $diesel_market; ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;"><b><?php echo $diesel_market_count; ?></b> Market Diesel</div>
						</div>
					</div>
				</div>
			</div>	
			
			<div class="col-md-3 col-sm-12">
				<div class="panel panel-blue panel-widget">
					<div class="row no-padding" style="border:0px solid #000">
						<div class="col-sm-4 widget-left">
							<svg class="glyph stroked clipboard with paper">
							<use xlink:href="#stroked-clipboard-with-paper"/></svg>

						</div>
						<div class="col-sm-8 widget-right">
							<div class="large" style="font-size:18px;">Rs.<?php echo $own_diesel; ?></div>
							<div class="text-muted" style="color:#000; font-size:13px; padding-top:6px;"><b><?php echo $own_diesel_count; ?></b> Own Truck Diesel</div>
						</div>
					</div>
				</div>
			</div>	  

	 <div class="form-group col-md-3 col-sm-12">
		<form method="post" action="show_fm.php" autocomplete="off" target="_blank">
					<label style="color:#000">Search FM By Id <font color="red"><sup>*</sup></font></label>
			   <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" type="text" name="idmemo" required />
			   <input value="FM" type="hidden" name="key" />
			  <button type="submit" name="submit" class="btn btn-sm btn-danger">Show Vou</button>
		</form>
     </div>
	
	<div class="form-group col-md-3 col-sm-12">
		<form method="post" action="show_fm.php" autocomplete="off" target="_blank">
					<label style="color:#000">Search FM By LRNo <font color="red"><sup>*</sup></font></label>
			   <input oninput="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" onblur="this.value=this.value.replace(/[^a-zA-Z0-9]/,'')" class="form-control" type="text" name="lrno" required />
			   <input value="LR" type="hidden" name="key" />
			  <button type="submit" name="submit" class="btn btn-sm btn-danger">Show Vou</button>
		</form>
     </div>
	 
      </div>
<br />

<div class="row">
		
	<div class="col-md-6 col-sm-12">
		<div class="panel panel-default chat" style="border:0px solid #000;">
<div style="color:#000;padding-top:5px;background:#06F;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:17px; border-bottom:1px solid #888; text-align:center;">
<i style="color:#FFF"><b>Market Truck</b> Summary</i></div>
	<div class="panel-body" style="overflow-x:hidden;">
<?php
$market_summary = mysqli_query($conn,"SELECT branch,fno,disamt,tno,type,dsl_nrr FROM diesel_fm WHERE pay_date='$today' AND disamt>0 ORDER BY id ASC");

echo    "<table class='table table-bordered' style='font-family:Verdana;color:#000;font-size:11px'>
		<tr>
		<th>Token</th>
		<th>Branch</th>
		<th>TruckNo</th>
		<th>Amount</th>
		<th>Narration</th>
		<th>Adv/Bal</th>
		</tr>";
if(mysqli_num_rows($market_summary)>0)
{
	while($row_market = mysqli_fetch_array($market_summary))
	  {
		echo"<tr>
			<td>$row_market[fno]</td>
			<td>$row_market[branch]</td>
			<td>$row_market[tno]</td>
			<td>$row_market[disamt]</td>
			<td>$row_market[dsl_nrr]</td>
			<td>$row_market[type]</td>
	</tr>";
	}
}
else
{
	echo"<tr>
			<td colspan='10'>No record found !</td>
		</tr>";
}	
echo  "</table>";
?>
					</div>
				</div>
			</div>
	
<div class="col-md-6 col-sm-12">
<div class="panel panel-default chat" style="border:0px solid #888;">
<div style="color:#000;padding-top:5px;background:#06F;font-family: 'Verdana', cursive; padding-bottom:5px; font-size:17px; border-bottom:1px solid #888; text-align:center;">
<i style="color:#FFF"><b>Own Truck</b> Summary</i></div>
<div class="panel-body" style="overflow-x:hidden;">
<?php
$own_summary = mysqli_query($conn_db,"SELECT tno,diesel,narration,branch FROM diesel_entry WHERE date='$today' AND card_pump='CARD' ORDER BY id ASC");

echo    "<table class='table table-bordered' style='font-family:Verdana;color:#000;font-size:11px'>
		<tr>
		<th>Branch</th>
		<th>TruckNo</th>
		<th>Amount</th>
		<th>Narration</th>
		</tr>";
if(mysqli_num_rows($own_summary)>0)
{
	while($row_own = mysqli_fetch_array($own_summary))
	  {
		echo"<tr>
			<td>$row_own[branch]</td>
			<td>$row_own[tno]</td>
			<td>$row_own[diesel]</td>
			<td>$row_own[narration]</td>
		</tr>";
	}
}
else
{
	echo"<tr>
			<td colspan='10'>No record found !</td>
		</tr>";
}	
echo  "</table>";
?>
					</div>
				</div>
			</div>
		</div>
</div>
</div>
</body>
</html>