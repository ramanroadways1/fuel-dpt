<?php
require_once 'connect.php';
?>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Fuel USER : RRPL</title>
<meta http-equiv="refresh" content="60">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/styles.css" rel="stylesheet">
<script src="js/lumino.glyphs.js"></script>

<style>
.form-control
{
	border:1px solid #000;
	background:#FFF;
	text-transform:uppercase;
}
</style>

 <style> 
 label{
	 font-family:Verdana;
	 font-size:13px;
	 color:#333;
 }
.table-bordered > tbody > tr > th {
     border: 1px solid #000;
}

.table-bordered > tbody > tr > td {
     border: 1px solid #000;
}
 </style> 
</head>

<body style="background:lightblue">

<?php include 'sidebar.php';?>

<div class="container-fluid;font-family:Verdana">	
	
<div class="col-sm-10 col-sm-offset-2 col-lg-10 col-lg-offset-2">			
	
	<div class="row">
		<div class="col-lg-12 col-sm-12 col-md-12">
		<br>
		<br>
				<h4 class="page-header" style="letter-spacing:1px;color:#000; font-size:20px; font-family: 'Verdana', cursive;">
				<center><b>Diesel - Statement </b></center></h4>
				<span color='#000'><center>(Approved Transactions)</center></span>
				<br />
		</div>
	</div>

		<div class="row" style="font-family:Verdana">
		
	<form action="diesel_statement.php" method="POST">		  
	
	<div class="form-group col-md-4 col-md-offset-4">
		<div class="form-group col-md-12">
			<label>From Date <font color="red">*</font></label>
			<input name="from" type="date" max="<?php echo date("Y-m-d"); ?>" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
		</div>
		
		<div class="form-group col-md-12">
			<label>To Date <font color="red">*</font></label>
			<input name="to" type="date" max="<?php echo date("Y-m-d"); ?>" class="form-control" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" />
		</div>
		
		<div class="form-group col-md-12">
			<label>Market/Own <font color="red">*</font></label>
			<select name="selection" class="form-control" required>
				<option value="M">MARKET TRUCK</option>
				<option value="O">OWN TRUCK</option>
			</select>
		</div>
		
		<div class="form-group col-md-12">
			<input type="submit" class="btn btn-block btn-danger" value="Download Statement" />
		</div>
	</form>	
     </div>
	 
      </div>
</div>
</div>
</body>
</html>