<?php 
require_once './connect.php';

$from = $_POST['from'];
$to = $_POST['to'];
$selection = $_POST['selection'];

$output = '';

if($selection=='M')
{
$query = mysqli_query($conn,"SELECT branch,fno,qty,rate,disamt,cash,tno,lrno,type,dsl_by,dcard,dcom,dsl_nrr,pay_date,done FROM diesel_fm 
WHERE pay_date BETWEEN '$from' AND '$to' AND approval='1' ORDER BY id ASC");

if(!$query)
{
	echo mysqli_error($conn);
	exit();
}

if(mysqli_num_rows($query) > 0)
 {
 $output .= '
   <table border="1">  
                    <tr>  
                         <th>Branch</th>  
                         <th>Token No</th>  
                         <th>TruckNo</th>  
                         <th>LR_No</th>  
                         <th>Qty</th>  
                         <th>Rate</th>  
                         <th>Diesel</th>  
                         <th>Cash</th>  
                         <th>Adv/Bal</th>  
                         <th>Card/Pump</th>  
                         <th>CardNo</th>  
                         <th>FuelCompany</th>  
                         <th>Narration</th>  
                         <th>Date</th>  
                         <th>Status</th>  
                    </tr>
  ';
  while($row = mysqli_fetch_array($query))
  {
	 if($row['done']==1)
	 {
		 $status="<b><font color='green'>Done</font></b>";
	 }
	else	 
	{
		$status="<b><font color='red'>Pending</font></b>";
	}
	  
   $output .= '
    <tr>  
							<td>'.$row["branch"].'</td>  
							<td>'.$row["fno"].'</td>  
							<td>'.$row["tno"].'</td>
						   <td>'.$row["lrno"].'</td>
						   <td>'.$row["qty"].'</td>
						   <td>'.$row["rate"].'</td>
						   <td>'.$row["disamt"].'</td>  
						   <td>'.$row["cash"].'</td>
						   <td>'.$row["type"].'</td>
						   <td>'.$row["dsl_by"].'</td>
						   <td>'."'".$row["dcard"].'</td>
						   <td>'.$row["dcom"].'</td>
						   <td>'.$row["dsl_nrr"].'</td>
						   <td>'.$row["pay_date"].'</td>
						   <td>'.$status.'</td>
					</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Diesel_Trans_Market.xls');
  echo $output;
  
}
 else
 {
	 echo "<script>
			alert('No result found..');
			window.close();
		</script>";
 }

}
else // IF OWN TRUCK
{
	$query = mysqli_query($conn_db,"SELECT diesel_entry.unq_id,diesel_entry.tno,diesel.qty,diesel.rate,diesel_entry.diesel,diesel_entry.card,
	diesel_entry.dsl_company,diesel_entry.narration,diesel_entry.branch,diesel_entry.date,diesel_entry.done FROM diesel,diesel_entry WHERE 
	diesel_entry.date BETWEEN '$from' AND '$to' AND (diesel_entry.card_pump='CARD' || diesel_entry.card_pump='OTP') AND diesel_entry.unq_id=diesel.unq_id ORDER BY diesel_entry.id ASC");

if(!$query)
{
	echo mysqli_error($conn_db);
	exit();
}

if(mysqli_num_rows($query) > 0)
 {
 $output .= '
   <table border="1">  
                    <tr>  
                         <th>Token No</th>  
						 <th>Branch</th>  	
						<th>Truck No</th>                          
						 <th>Qty</th>  
						 <th>Rate</th>  
						 <th>Amount</th>  
                        <th>Card_No</th>  
                        <th>FuelCompany</th>  
                         <th>Narration</th>  
                         <th>Date</th>  
                         <th>Status</th>  
                    </tr>
  ';
  while($row = mysqli_fetch_array($query))
  {
	 if($row['done']==1)
	 {
		 $status="<b><font color='green'>Done</font></b>";
	 }
	else	 
	{
		$status="<b><font color='red'>Pending</font></b>";
	}
	
   $output .= '
    <tr>  
							<td>'.$row["unq_id"].'</td>  
							<td>'.$row["branch"].'</td>  
							<td>'.$row["tno"].'</td>
						   <td>'.$row["qty"].'</td>
						   <td>'.$row["rate"].'</td>
						   <td>'.$row["diesel"].'</td>
						   <td>'."'".$row["card"].'</td>
						   <td>'.$row["dsl_company"].'</td>
						   <td>'.$row["narration"].'</td>
						   <td>'.$row["date"].'</td>
						   <td>'.$status.'</td>
					</tr>
   ';
  }
  $output .= '</table>';
  header('Content-Type: application/xls');
  header('Content-Disposition: attachment; filename=Diesel_Trans_Own.xls');
  echo $output;
  
}
 else
 {
	 echo "<script>
			alert('No result found..');
			window.close();
		</script>";
 }
}
?>